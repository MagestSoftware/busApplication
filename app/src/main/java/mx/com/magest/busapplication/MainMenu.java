package mx.com.magest.busapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;

public class MainMenu extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    public void ScanQR(View view){
        startActivity(new Intent(MainMenu.this, QrCamera.class));
    }

    public void ShowMap(View view){
        startActivity(new Intent(MainMenu.this, MapView.class));
    }

    public void ShowProfile(View view){
        startActivity(new Intent(MainMenu.this, Profile.class));
    }

    public void ShowAbout(View view){
        startActivity(new Intent(MainMenu.this, About.class));
    }

}
